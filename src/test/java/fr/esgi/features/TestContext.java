package fr.esgi.features;

import fr.esgi.domain.city.City;
import fr.esgi.domain.city.CityLink;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TestContext {

    final Map<String, City> cities = new HashMap<>();
    final List<CityLink> cityLinks = new ArrayList<>();
}
