package fr.esgi.features;

import fr.esgi.domain.city.City;
import io.cucumber.java8.En;
import org.assertj.core.api.Assertions;
import org.assertj.core.util.Maps;

import java.util.Map;

public class InfectionSteps implements En {

    public InfectionSteps(TestContext testContext) {
        Given("{cityName} has not been infected", (String cityName) -> {
            testContext.cities.get(cityName).setInfectionLevel(0);
        });
        When("{cityName} is infected", (String cityName) -> {
            testContext.cities.get(cityName).infect();
        });
        Then("{cityName}' infection level should be( increase to) {int}",
                (cityName, infectionLevel) -> {
                    Assertions.assertThat(testContext.cities.get(cityName).getInfectionLevel()).isEqualTo(infectionLevel);
                });
        Given("{cityName} has been infected {int} time(s)",
                (cityName, infectionTimes) -> {
                    for (int i = 0; i < (int) infectionTimes; i++) {
                        testContext.cities.get(cityName).infect();
                    }
                }
        );
    }
}
