package fr.esgi.features;

import fr.esgi.domain.city.City;
import fr.esgi.domain.city.CityLink;
import io.cucumber.datatable.DataTable;
import io.cucumber.java8.En;
import io.cucumber.java8.StepDefinitionBody.A2;
import org.assertj.core.api.Assertions;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;

public class NetworkSteps implements En {

    final private TestContext testContext;

    public NetworkSteps(TestContext testContext) {
        this.testContext = testContext;

        ParameterType("cityName", ".*", (String cityName) -> cityName);

        ParameterType("cityList", ".*", (String list) ->
                Arrays.stream(list.split(",")).map(name -> new City(name.trim())).collect(Collectors.toList()));

        Given("the occident network", () -> {
            testContext.cities.put("Paris", new City("Paris"));
            testContext.cities.put("Essen", new City("Essen"));
            testContext.cities.put("London", new City("London"));
            testContext.cities.put("Algiers", new City("Algiers"));
            testContext.cities.put("Madrid", new City("Madrid"));
            testContext.cities.put("Milan", new City("Milan"));
            testContext.cities.put("New_york", new City("New_york"));

            testContext.cityLinks.add(new CityLink("Paris", "London"));
            testContext.cityLinks.add(new CityLink("Paris", "Essen"));
            testContext.cityLinks.add(new CityLink("Paris", "Milan"));
            testContext.cityLinks.add(new CityLink("Paris", "Algiers"));
            testContext.cityLinks.add(new CityLink("Paris", "Madrid"));

            testContext.cityLinks.add(new CityLink("Essen", "London"));
            testContext.cityLinks.add(new CityLink("Essen", "Milan"));

            testContext.cityLinks.add(new CityLink("Madrid", "London"));
            testContext.cityLinks.add(new CityLink("Madrid", "Algiers"));
        });

        Then("the cities should have the following infection levels:", (DataTable dataTable) -> {
            List<Map<String, String>> maps = dataTable.asMaps();
            maps.forEach(map -> {
                String cityName = map.get("cityName");
                assertThat(testContext.cities).containsKey(cityName);
                assertThat(testContext.cities.get(cityName).getInfectionLevel()).isEqualTo(Integer.parseInt(map.get("infection level")));
            });
        });

        And("{cityName} should be linked to {cityList}", (A2<String, List<City>>) (cityName, linkedCities) ->
                assertThat(linksContaining(cityName))
                    .containsExactlyInAnyOrderElementsOf(links(cityName, linkedCities)));
    }

    private List<CityLink> links(String cityName, List<City> linkedCities) {
        return linkedCities.stream().map(city -> new CityLink(cityName, city.getCityName())).toList();
    }

    private Stream<CityLink> linksContaining(String cityName) {
        return testContext.cityLinks.stream().filter(cityLink -> cityLink.contains(cityName));
    }

}
