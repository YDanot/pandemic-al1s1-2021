package fr.esgi.domain.city;


import org.assertj.core.api.Assertions;
import org.junit.Test;

public class CityLinkTest {


    @Test
    public void cityLinkEquals() {
        CityLink parisEssen = new CityLink("Paris", "Essen");
        CityLink essenParis = new CityLink("Essen", "Paris");

        Assertions.assertThat(parisEssen).isEqualTo(essenParis);
    }
}