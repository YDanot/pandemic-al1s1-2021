Feature: Infection of a city

  Scenario: First infection
    Given the occident network
    Given Paris has not been infected
    When Paris is infected
    Then Paris' infection level should be increase to 1

  Scenario: Second infection
    Given the occident network
    Given Paris has been infected 1 time
    When Paris is infected
    Then Paris' infection level should be increase to 2

  Scenario: Third infection
    Given the occident network
    Given Paris has been infected 2 times
    When Paris is infected
    Then Paris' infection level should be increase to 3

  Scenario: Fourth infection
    Given the occident network
    Given Paris has been infected 3 times
    When Paris is infected
    Then Paris' infection level should be 3