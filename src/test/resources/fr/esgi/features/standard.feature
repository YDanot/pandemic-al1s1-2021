Feature: Standard definition

  Scenario: Occident network definition

#             (LONDON)------------------(ESSEN)
#               | \                     /  |
#               |    \                /    |
#               |       \           /      |
#  (NEW-YORK)   |          (PARIS)         |
#               |       /    |     \       |
#               |    /       |        \    |
#               | /          |           \ |
#            (MADRID)        |          (MILAN)
#                \           |
#                   \        |
#                      \     |
#                         \  |
#                         (ALGIERS)

    Given the occident network
    Then the cities should have the following infection levels:
      | cityName | infection level |
      | Paris    | 0               |
      | London   | 0               |
      | Essen    | 0               |
      | Algiers  | 0               |
      | Madrid   | 0               |
      | Milan    | 0               |
      | New_york | 0               |
    And Paris should be linked to London, Essen, Milan, Algiers, Madrid
    And Essen should be linked to London, Paris, Milan
    And Madrid should be linked to London, Paris, Algiers