package fr.esgi.domain.city;

import java.util.Objects;

public class CityLink {

    private final String cityName1;
    private final String cityName2;

    public CityLink(String cityName1, String cityName2) {
        this.cityName1 = cityName1;
        this.cityName2 = cityName2;
    }

    public String getCityName1() {
        return cityName1;
    }

    public String getCityName2() {
        return cityName2;
    }

    @Override
    public String toString() {
        return "CityLink{" +
                "cityName1='" + cityName1 + '\'' +
                ", cityName2='" + cityName2 + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CityLink cityLink = (CityLink) o;
        return Objects.equals(cityName1, cityLink.cityName1) && Objects.equals(cityName2, cityLink.cityName2)
        || Objects.equals(cityName1, cityLink.cityName2) && Objects.equals(cityName2, cityLink.cityName1);
    }

    @Override
    public int hashCode() {
        return Objects.hash(cityName1, cityName2);
    }

    public boolean contains(String cityName) {
        return Objects.equals(cityName1, cityName) || Objects.equals(cityName2, cityName);
    }
}
