package fr.esgi.domain.city;

public class City {
    private final String cityName;
    private int infectionLevel;

    public City(String cityName) {
        this.cityName = cityName;
    }

    public String getCityName() {
        return cityName;
    }

    public void setInfectionLevel(int infectionLevel) {
        this.infectionLevel = infectionLevel;
    }

    public int getInfectionLevel() {
        return infectionLevel;
    }

    public void infect() {
        if (infectionLevel < 3) {
            setInfectionLevel(infectionLevel + 1);
        }
    }

    @Override
    public String toString() {
        return "City{" +
                "cityName='" + cityName + '\'' +
                ", infectionLevel=" + infectionLevel +
                '}';
    }
}
